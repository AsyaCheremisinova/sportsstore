
    let thumb = document.querySelector('.new-project__thumb');
    let slider = document.querySelector('.new-project__slider');
    let cost = document.querySelector('.new-project__cost');
    let oldLeft = 0;
    let c = 1000000;
    


    thumb.onmousedown = function(event) {
      event.preventDefault(); // предотвратить запуск выделения (действие браузера)

      let shiftX = event.clientX - thumb.getBoundingClientRect().left;
      // shiftY здесь не нужен, слайдер двигается только по горизонтали

      document.addEventListener('mousemove', onMouseMove);
      document.addEventListener('mouseup', onMouseUp);

      function onMouseMove(event) {
        let newLeft = event.clientX - shiftX - slider.getBoundingClientRect().left;
        

        // курсор вышел из слайдера => оставить бегунок в его границах.
        if (newLeft < 0) {
          newLeft = 0;
        }
        let rightEdge = slider.offsetWidth - thumb.offsetWidth;
        if (newLeft > rightEdge) {
          newLeft = rightEdge;
        }

        thumb.style.left = newLeft + 'px';
        cost.style.left = newLeft + 'px';
        console.log(newLeft);

        if(newLeft-oldLeft >= 30)
        {
            c = c + 500000;
            cost.innerHTML = c;
            oldLeft = newLeft;
        }
        if(oldLeft-newLeft >= 30)
        {
            c = c - 500000;
            cost.innerHTML = c;
            oldLeft = newLeft;            
        }
      }

      function onMouseUp() {
        document.removeEventListener('mouseup', onMouseUp);
        document.removeEventListener('mousemove', onMouseMove);
      }

    };

    thumb.ondragstart = function() {
      return false;
    };

  