let sliderline = document.querySelector('.features__slider-line');
let features__slider = document.querySelector('.features__slider');

    sliderline.onmousedown = function(event) {
      event.preventDefault();

      let shiftX = event.clientX - sliderline.getBoundingClientRect().left;

      document.addEventListener('mousemove', onMouseMove);
      document.addEventListener('mouseup', onMouseUp);

      function onMouseMove(event) {
        let newLeft = event.clientX - shiftX - features__slider.getBoundingClientRect().left;

        if (newLeft < 100) {
          newLeft = 100;
        }
        let rightEdge = features__slider.offsetWidth - sliderline.offsetWidth;
        if (newLeft > rightEdge) {
          newLeft = rightEdge;
        }

        sliderline.style.left = newLeft + 'px';
      }

      function onMouseUp() {
        document.removeEventListener('mouseup', onMouseUp);
        document.removeEventListener('mousemove', onMouseMove);
      }

    };

    sliderline.ondragstart = function() {
      return false;
    };


    
    function onEntry(entry) {
      entry.forEach(change => {
        if (change.isIntersecting) {
         change.target.classList.add('features__element-show');
        }
      });
    }
    
    let option = {
      threshold: [0.5] };
    let observer2 = new IntersectionObserver(onEntry, option);
    let element = document.querySelectorAll('.features__animation');
  
    
    for (let elm of element) {
      observer2.observe(elm);
    }