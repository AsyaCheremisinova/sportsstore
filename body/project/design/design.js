let slider_line = document.querySelector('.design__slider-line');
let slider = document.querySelector('.design__slider');

    slider_line.onmousedown = function(event) {
      event.preventDefault(); 

      let shiftX =  slider_line.getBoundingClientRect().right - event.clientX;

      document.addEventListener('mousemove', onMouseMove);
      document.addEventListener('mouseup', onMouseUp);

      function onMouseMove(event) {
        let newLeft = event.clientX - shiftX - slider.getBoundingClientRect().left;  

        if (newLeft > 70) {
            newLeft = 70;
        }
        if (newLeft < 0) {
            newLeft = -300;
        }
        slider_line.style.left = newLeft + 'px';

      }

      function onMouseUp() {
        document.removeEventListener('mouseup', onMouseUp);
        document.removeEventListener('mousemove', onMouseMove);
      }

    };

    slider_line.ondragstart = function() {
      return false;
    };



    function onEntry(entry) {
      entry.forEach(change => {
        if (change.isIntersecting) {
         change.target.classList.add('design__element-show');
        }
      });
    }
    
    let options3 = {
      threshold: [0.5] };
    let observer3 = new IntersectionObserver(onEntry, options3);
    let elements3 = document.querySelectorAll('.design__animation');
  
    
    for (let elm of elements3) {
      observer3.observe(elm);
    }