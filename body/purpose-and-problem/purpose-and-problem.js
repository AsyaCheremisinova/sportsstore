let offset = 0;
const sliderLine = document.querySelector('.purpose-and-problem__slider-line');
const button_next = document.querySelector('.purpose-and-problem__slider-next');
const button_prev = document.querySelector('.purpose-and-problem__slider-prev');


document.querySelector('.purpose-and-problem__slider-next').addEventListener('click', function(){
    offset = offset + 100;
    button_next.style.overflow = "hidden";
    button_next.style.height = 0;
    button_next.style.opacity = 0;

    button_prev.style.overflow = "visible";
    button_prev.style.height = "15%";
    button_prev.style.opacity = 1;

    if (offset > 100) {
        offset = 0;

    }    
    sliderLine.style.left = -offset + '%';
  
});

document.querySelector('.purpose-and-problem__slider-prev').addEventListener('click', function () {
    offset = offset - 100;
    button_prev.style.overflow = "visible";
    button_prev.style.height = 0;
    button_prev.style.opacity = 0;

    button_next.style.overflow = "hidden";
    button_next.style.height = "15%";
    button_next.style.opacity = 1;
    if (offset < 0) {
        offset = 100;
    }
    sliderLine.style.left = -offset + '%';
});